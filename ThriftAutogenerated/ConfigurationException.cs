﻿using System;

namespace Shared
{
    [Serializable]
    public class ConfigurationException : Exception
    {
        public ConfigurationException()
        {
        }

        public ConfigurationException(string message)
            : base(message)
        {
        }

        public ConfigurationException(string property, string message)
            : base(property + " " + message)
        {
            Property = property;
        }

        public ConfigurationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public string Property { get; }
    }
}