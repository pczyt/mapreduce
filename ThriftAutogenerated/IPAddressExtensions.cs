﻿using System.Net;
using System.Net.Sockets;

namespace Common
{
    public static class IPAddressExtensions
    {
        public static bool IsMulticast(this IPAddress ip)
        {
            var tmp = ip.GetAddressBytes()[0] >> 4;
            return tmp == 14;
        }

        public static bool IsIpv4(this IPAddress ip)
        {
            return ip.AddressFamily == AddressFamily.InterNetwork;
        }
    }
}