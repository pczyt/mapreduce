﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace Master.Configuration
{
    public class JsonConfigurationProvider : IConfigurationProvider
    {
        private readonly string _jsonString;

        public JsonConfigurationProvider(string jsonString)
        {
            _jsonString = jsonString;
        }

        public JsonConfigurationProvider()
        {
        }

        public IConfiguration GetConfiguration()
        {
            return GetConfiguration(_jsonString);
        }

        public IConfiguration GetConfiguration(object data)
        {
            if (!(data is string jsonString))
                throw new ArgumentException("Incorrect type of data for JsonConfiguration. Expected string");
            var tmpConf = JsonConvert.DeserializeObject<ConfigurationEntity>(jsonString, new IpAddressConverter());
            tmpConf.Validate();
            return tmpConf;
        }

        #region JSON Converter

        private class IpAddressConverter : JsonConverter<IPAddress>
        {
            public override void WriteJson(JsonWriter writer, IPAddress value, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override IPAddress ReadJson(JsonReader reader, Type objectType, IPAddress existingValue,
                bool hasExistingValue,
                JsonSerializer serializer)
            {
                var obj = IPAddress.Parse((string) reader.Value);
                return obj;
            }
        }

        #endregion
    }
}