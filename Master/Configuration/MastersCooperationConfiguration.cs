﻿using System.Net;
using Common;
using Shared;

namespace Master.Configuration
{
    public class MastersCooperationConfiguration
    {
        public MastersCooperationConfiguration(IPAddress multicastGroupAddress, int multicastGroupPort)
        {
            MulticastGroupAddress = multicastGroupAddress;
            MulticastGroupPort = multicastGroupPort;
        }

        public int MulticastGroupPort { get; }

        public IPAddress MulticastGroupAddress { get; }

        public void Validate()
        {
            if (MulticastGroupAddress is null)
                throw new ConfigurationException("MastersCooperationConfiguration.MulticastGroupAddress",
                    "can't be null");

            //sprawdzenie czy adres jest ipv4, oraz czy jego poczatkowe bity wskazuja na adres typu multicast
            if (!MulticastGroupAddress.IsIpv4() || !MulticastGroupAddress.IsMulticast())
                throw new ConfigurationException("MastersCooperationConfiguration.MulticastGroupAddress",
                    "must be IPv4 Multicast Address");
            if (MulticastGroupPort < 1024 || MulticastGroupPort > 65535)
                throw new ConfigurationException(
                    "MastersCooperationConfiguration.MulticastGroupPort should be in range 1024-65535");
        }

        public override string ToString()
        {
            return $"MastersCooperationConfiguration:\n\tMulticastGroupAddres:{MulticastGroupAddress}";
        }
    }
}