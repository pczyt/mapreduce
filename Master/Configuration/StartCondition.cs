﻿namespace Master.Configuration
{
    public enum StartCondition
    {
        WorkersNumber = 1,
        Time = 2,
        WorkersNumberAndTime = 3
    }
}