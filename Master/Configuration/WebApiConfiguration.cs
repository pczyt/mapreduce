﻿using System.IO;
using Shared;

namespace Master.Configuration
{
    public class WebApiConfiguration
    {
        public WebApiConfiguration(string login, string password, string certificatePath, string host, int port)
        {
            Login = login;
            Password = password;
            Port = port;
            CertificatePath = certificatePath;
            Host = host;
        }

        public string Host { get; }

        public string CertificatePath { get; }

        public int Port { get; }

        public string Password { get; }

        public string Login { get; }
        public string CertificatePassword { get; set; }

        public void Validate()
        {
            if (!File.Exists(CertificatePath))
                throw new ConfigurationException("WebApiConfiguration.CertificatePath", "file not found");
            if (Port < 1 || Port > 65535)
                throw new ConfigurationException("WebApiConfiguration.Port is invalid");
            foreach (var property in GetType().GetProperties())
                if (property.Name != "CertificatePassword" && property.GetValue(this) == null)
                    throw new ConfigurationException($"WebApiConfiguration.{property.Name}", "can not be null");
        }
    }
}