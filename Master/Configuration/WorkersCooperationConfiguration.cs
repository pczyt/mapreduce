﻿using System.Net;
using Common;
using Newtonsoft.Json;
using Shared;

namespace Master.Configuration
{
    public class WorkersCooperationConfiguration
    {
        [JsonConstructor]
        public WorkersCooperationConfiguration(IPAddress multicastGroupAddress, int multicastGroupPort,
            StartCondition startCondition,
            int? minWorkers, int? waitTime)
        {
            MulticastGroupAddress = multicastGroupAddress;
            MulticastGroupPort = multicastGroupPort;
            StartCondition = startCondition;
            MinWorkers = minWorkers;
            WaitTime = waitTime;
        }

        public IPAddress MulticastGroupAddress { get; }
        public int MulticastGroupPort { get; }
        public StartCondition StartCondition { get; }
        public int? MinWorkers { get; }
        public int? WaitTime { get; }

        public void Validate()
        {
            ValidateStartCondition();
            ValidateMulticastGroupAddress();
            ValidateMulticastPort();
        }

        private void ValidateMulticastPort()
        {
            if (MulticastGroupPort < 1024 || MulticastGroupPort > 65535)
                throw new ConfigurationException(
                    "WorkersCooperationConfiguration.MulticastGroupPort should be in range 1024-65535");
        }

        public override string ToString()
        {
            return
                $"WorkersCooperationConfiguration:\n\tMulticastGroupAddres:{MulticastGroupAddress}\n\tStartCondition:{StartCondition}\n\tMinWorkers:{MinWorkers}\n\tWaitTime={WaitTime}";
        }

        #region Validate Helpers

        public void ValidateMulticastGroupAddress()
        {
            if (MulticastGroupAddress is null)
                throw new ConfigurationException("WorkersCooperationConfiguration.MulticastGroupAddress",
                    "can't be null");

            //sprawdzenie czy adres jest ipv4, oraz czy jego poczatkowe bity wskazuja na adres typu multicast
            if (!MulticastGroupAddress.IsIpv4() || !MulticastGroupAddress.IsMulticast())
                throw new ConfigurationException(
                    "WorkersCooperationConfiguration.MulticastGroupAddress", "must be IPv4 Multicast Address");
        }

        private void ValidateStartCondition()
        {
            //sprawdzenie czy ustawiono minimalna ilosc workerow
            if (((uint) StartCondition & 1) == 1)
                if (!MinWorkers.HasValue)
                    throw new ConfigurationException("MinWorkers",
                        "For start-condition:WorkersNumber or start-condition:WorkersNumberAndTime you must define workers-wait-time");

            //sprawdzenie czy ustawiono minimalny czas, oraz czy jest on dodatni
            if (((uint) StartCondition & 2) == 2)
            {
                if (!WaitTime.HasValue)
                    throw new ConfigurationException("WaitTime",
                        "For start-condition:TIME or start-condition:WORKERS_NUMBER_AND_TIME you must define workers-wait-time");
                if (WaitTime.Value < 1)
                    throw new ConfigurationException("WaitTime",
                        "For start-condition:TIME or start-condition:WORKERS_NUMBER_AND_TIM you must define positive workers-wait-time");
            }
        }

        #endregion
    }
}