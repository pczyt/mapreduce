﻿using System.IO;
using System.Net;
using Common;
using Shared;

namespace Master.Configuration
{
    public class GeneralConfiguration
    {
        public GeneralConfiguration(IPAddress ip, int port, string dataStoragePath, string shellPath)
        {
            Ip = ip;
            Port = port;
            DataStoragePath = dataStoragePath;
            ShellPath = shellPath;
        }

        public string DataStoragePath { get; set; }
        public string ShellPath { get; set; }
        public int Port { get; }

        public IPAddress Ip { get; }


        public void Validate()
        {
            if (Ip is null) throw new ConfigurationException("GeneralConfiguration.Ip", "can't be null");

            //sprawdzenie czy adres jest ipv4, oraz czy jego poczatkowe bity nie wskazuja na adres multicast
            if (!Ip.IsIpv4() || Ip.IsMulticast())
                throw new ConfigurationException("GeneralConfiguration.Ip", "must be IPv4 Multicast Address");
            if (Port < 1024 || Port > 65535)
                throw new ConfigurationException("GeneralConfiguration.Port", "should be in range 1024-65535");
            if(!Directory.Exists(DataStoragePath))
                throw new ConfigurationException("GeneralConfiguration.DataStoragePath", "directory not found");
     /*       if(!File.Exists(ShellPath))
                throw new ConfigurationException("GeneralConfiguration.ShellPath", "file not found");
        */}

        public override string ToString()
        {
            return $"GeneralConfiguration:\n\tIp:{Ip}\nPort:{Port}\nDataStoragePath:{DataStoragePath}";
        }
    }
}