﻿using Newtonsoft.Json;
using Shared;

namespace Master.Configuration
{
    public class ConfigurationEntity : IConfiguration
    {
        [JsonProperty] public WorkersCooperationConfiguration WorkersCooperationConfiguration { get; private set; }

        [JsonProperty] public MastersCooperationConfiguration MastersCooperationConfiguration { get; private set; }

        [JsonProperty] public GeneralConfiguration GeneralConfiguration { get; private set; }
        [JsonProperty] public WebApiConfiguration WebApiConfiguration { get; private set; }

        public void Validate()
        {
            if (WorkersCooperationConfiguration is null)
                throw new ConfigurationException("ConfigurationEntity.WorkersCooperationConfiguration",
                    "can't be null");

            if (MastersCooperationConfiguration is null)
                throw new ConfigurationException("ConfigurationEntity.MastersCooperationConfiguration",
                    "can't be null");

            if (GeneralConfiguration is null)
                throw new ConfigurationException("ConfigurationEntity.GeneralConfiguration", "can't be null");
            WorkersCooperationConfiguration.Validate();
            MastersCooperationConfiguration.Validate();
            GeneralConfiguration.Validate();
            WebApiConfiguration.Validate();
        }

        public override string ToString()
        {
            return
                $"ConfigurationEntity:\n{GeneralConfiguration}\n{MastersCooperationConfiguration}\n{WorkersCooperationConfiguration}\n{WebApiConfiguration}";
        }
    }
}