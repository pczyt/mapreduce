﻿namespace Master.Configuration
{
    public interface IConfiguration
    {
        WorkersCooperationConfiguration WorkersCooperationConfiguration { get; }

        MastersCooperationConfiguration MastersCooperationConfiguration { get; }
        WebApiConfiguration WebApiConfiguration { get; }

        GeneralConfiguration GeneralConfiguration { get; }

        void Validate();
    }
}