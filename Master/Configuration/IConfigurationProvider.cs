﻿namespace Master.Configuration
{
    public interface IConfigurationProvider
    {
        IConfiguration GetConfiguration();
        IConfiguration GetConfiguration(object data);
    }
}