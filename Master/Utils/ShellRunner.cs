﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Master.Common;

namespace Master.Utils
{
    public static class ShellRunner
    {
        
        public static List<string> Run(string cmd, string shellPath)
        {
            var process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo =
                new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                    FileName = shellPath,
                    Arguments = $"-c \"{cmd}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false,
                    CreateNoWindow = false
                };
            process.StartInfo = startInfo;
            var results = new List<string>();
    process.OutputDataReceived += (sender, args) =>
    {
         if (args.Data == null) return;
        results.Add(args.Data);
    };
    process.Start();
            
    process.BeginOutputReadLine();
            process.WaitForExit();
            return results;

        }

      

       
    }

   
}