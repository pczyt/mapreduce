﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MapReduce.Thrift;
using Master.Workflow.ThriftCommunication;

namespace Master.Utils
{
    public static class ConnectedClientsDictionaryExtensions
    {
        public static List<ClientListeningInfo> GetClientListeningInfos(
            this ConcurrentDictionary<int, StateOfClient> dict)
        {
            return dict.Select(x =>
                    new ClientListeningInfo(BitConverter.ToInt32(x.Value.Ip.GetAddressBytes(), 0),
                        x.Value.ListeningPort))
                .ToList();
        }
    }
}