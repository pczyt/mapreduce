﻿namespace Master.Utils
{
    public static class StringExtensions
    {
        //implementacja z https://msdn.microsoft.com/en-us/library/ekd1t784(v=vs.110).aspx
        public static byte[] ToByteArray(this string msg)
        {
            var ret = new byte[msg.Length];
            for (var i = 0; i < msg.Length; i++)
                ret[i] = (byte) msg[i];
            return ret;
        }
    }
}