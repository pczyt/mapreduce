﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Master.Workflow.Messages;
using Microsoft.Extensions.Logging;

namespace Master.Workflow
{
    public class WorkerThread
    {
        private readonly ILogger<WorkerThread> _logger;
        private readonly BlockingCollection<MasterWorkflowMessages> _masterQueue;
        private readonly BlockingCollection<Action<CancellationToken>> _queue;

        public WorkerThread(ILogger<WorkerThread> logger, BlockingCollection<Action<CancellationToken>> queue,
            BlockingCollection<MasterWorkflowMessages> masterQueue)
        {
            _logger = logger;
            _queue = queue;
            _masterQueue = masterQueue;
        }

        public void Start(CancellationToken token)
        {
            _logger.LogTrace("Starting threadworker loop");
            try
            {
                foreach (var action in _queue.GetConsumingEnumerable(token))
                {
                    _logger.LogTrace("Running next action");
                    action(token);
                    _masterQueue.Add(MasterWorkflowMessages.StateFinished);
                }
            }
            catch (OperationCanceledException)
            {
                _logger.LogTrace("Wyszedlem z petli bo otrzymalem zadanie zatrzymania.");
            }
            catch (Exception ex)
            {
                _logger.LogDebug($"Wystapil wyjatek {ex.Message}\n{ex.StackTrace}");
                throw;
            }
        }
    }
}