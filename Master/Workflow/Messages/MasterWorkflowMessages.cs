﻿namespace Master.Workflow.Messages
{
    public enum MasterWorkflowMessages
    {
        StateFinished,
        Start,
        Abort
    }
}