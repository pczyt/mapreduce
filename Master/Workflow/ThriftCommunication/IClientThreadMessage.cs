﻿namespace Master.Workflow.ThriftCommunication
{
    public interface IClientThreadMessage
    {
        int ServerThreadId { get; }
    }
}