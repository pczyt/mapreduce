﻿using System;
using MapReduce.Thrift;

namespace Master.Workflow.ThriftCommunication
{
    public class SystemTask<T> where T : MapReduceBase
    {
        private readonly object[] _args;

        public SystemTask(string methodName, params object[] args)
        {
            MethodName = methodName;
            _args = args;
        }

        public string MethodName { get; }

        public object Do(T client)
        {
            var thisType = typeof(T);
            var theMethod = thisType.GetMethod(MethodName);
            try
            {
                var result = theMethod.Invoke(client, _args);
                return result;
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                #endif

                throw;
            }
        }
    }
}