﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using MapReduce.Thrift;
using Master.Common;
using Microsoft.Extensions.Logging;
using Thrift.Protocol;
using Thrift.Transport;

namespace Master.Workflow.ThriftCommunication
{
    public class ClientThread
    {
        private readonly IPAddress _clientIp;
        private readonly int _clientPort;
        private readonly BlockingCollection<IClientThreadMessage> _clientsRunnerQueue;
        private readonly ILogger<ClientThread> _logger;
        private readonly BlockingCollection<SystemTask<MapReduceWorker.Client>> _queue;
        private readonly int _serverThreadId;

        public ClientThread(ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> dict,
            NewClientThreadMessage element, BlockingCollection<IClientThreadMessage> clientsRunnerQueue,
            ILogger<ClientThread> logger, ApplicationState appState)
        {
            _queue = dict[element.ServerThreadId];
            _serverThreadId = element.ServerThreadId;
            _clientIp = element.ClientIp;
            _clientPort = element.ClientPort;
            _clientsRunnerQueue = clientsRunnerQueue;
            _logger = logger;
        }

        public void Start(CancellationToken token)
        {
            ;
            TTransport transport = null;
            try
            {
                transport = new TSocket(new TcpClient(AddressFamily.InterNetwork));
                //bardzo brzydki hack zeby nie modyfikowac biblioteki
                //bez tego nie dzialalo dla adresow ipv4, bo albo mozna bylo wstawic swoj tcpclient
                //albo ustawic host i port, nie dalo sie ustawic tego i tego
                var prop = transport.GetType().GetField("host", BindingFlags.NonPublic
                                                                | BindingFlags.Instance);
                prop.SetValue(transport, _clientIp.ToString());
                var prop2 = transport.GetType().GetField("port", BindingFlags.NonPublic
                                                                 | BindingFlags.Instance);
                prop2.SetValue(transport, _clientPort);


                TProtocol protocol = new TBinaryProtocol(transport);
                _logger.LogTrace($"Otwieram polaczenie do {_clientIp}:{_clientPort} dla {_serverThreadId}");
                var client = new MapReduceWorker.Client(protocol);
                transport.Open();
                //jak jestesmy tu to znaczy, ze polaczenie sie otworzylo
                _clientsRunnerQueue.Add(new ConfirmedClientMessage(_serverThreadId));
                _logger.LogTrace($"Czekam na polecenia dla {_serverThreadId}");
                foreach (var item in _queue.GetConsumingEnumerable(token))
                {
                    _logger.LogDebug($"Wywoluje {item.MethodName} na {_clientIp}");
                    item.Do(client);
                }
            }
            catch (OperationCanceledException ocx)
            {
                _logger.LogTrace($"Zamykam watek kliencki dla {_serverThreadId}");
            }
            catch (Exception ex)
            {
                _logger.LogDebug($"Rozlaczono klienta {_clientIp}");
                _clientsRunnerQueue.Add(new DisconnectedClientThreadMessage(_serverThreadId));
            }
            finally
            {
                transport?.Close();
            }
        }
    }
}