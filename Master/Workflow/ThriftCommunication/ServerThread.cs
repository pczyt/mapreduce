﻿using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;
using MapReduce.Thrift;
using Master.Common;
using Microsoft.Extensions.Logging;
using Thrift.Server;
using Thrift.Transport;

namespace Master.Workflow.ThriftCommunication
{
    public class ServerThread
    {
        private readonly ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> _dict;
        private readonly MapReduceMasterHandler _handler;
        private readonly ILogger<ServerThread> _logger;
        private readonly BlockingCollection<IClientThreadMessage> _newClientsThreadsQueue;
        private readonly ApplicationState _state;
        private TcpListener _listener;
        private TServerSocket _serverTransport;
        private TThreadedServer _threadedServer;


        public ServerThread(ILogger<ServerThread> logger, ApplicationState state, MapReduceMasterHandler handler,
            ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> dict,
            BlockingCollection<IClientThreadMessage> newClientsThreadsQueue)
        {
            _logger = logger;
            _handler = handler;
            _dict = dict;
            _newClientsThreadsQueue = newClientsThreadsQueue;
            _state = state;
        }

        public void Start(CancellationToken token)
        {
            var clientsThreadRunner = new Thread(() => ClientThreadsRunner(token));
            clientsThreadRunner.Start();
            try
            {
                var processor = new MapReduceMaster.Processor(_handler);
                _listener = new TcpListener(_state.Configuration.GeneralConfiguration.Ip,
                    _state.Configuration.GeneralConfiguration.Port);
                var serverTransport = new TServerSocket(_listener);

                _threadedServer = new TThreadedServer(processor, serverTransport, s => { });
                _logger.LogTrace("Rozpoczecie przyjmowania polaczen od klientow...");
                _threadedServer.Serve();
            }
            catch (TTransportException ex)
            {
                if (ex.InnerSocketException.ErrorCode == 10004) _logger.LogDebug("Koniec nasluchiwania.");
            }
            catch (Exception x)
            {
                _logger.LogDebug(x.StackTrace);
            }
        }

        public void CleanUp()
        {
            _threadedServer.Stop();

            // _listener?.Stop();
        }

        private void ClientThreadsRunner(CancellationToken token)
        {
            try
            {
                foreach (var element in _newClientsThreadsQueue.GetConsumingEnumerable(token))
                    switch (element)
                    {
                        case DisconnectedClientThreadMessage disconnectedMessage:
                            _state.CurrentState.HandleDisconnectedClient(disconnectedMessage);
                            break;
                        case NewClientThreadMessage newClientMessage:
                            _state.CurrentState.HandleNewClient(token, newClientMessage, element);
                            break;
                        case ConfirmedClientMessage confirmedMessage:
                            _state.CurrentState.HandleConfirmedMessage(confirmedMessage);
                            break;
                    }
            }
            catch (OperationCanceledException)
            {
                _logger.LogTrace($"Koncze ClientThreadsRunner");
            }
        }
    }
}