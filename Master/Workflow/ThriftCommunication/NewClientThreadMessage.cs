﻿using System.Net;

namespace Master.Workflow.ThriftCommunication
{
    public class NewClientThreadMessage : IClientThreadMessage
    {
        public NewClientThreadMessage(int threadId, IPAddress clientIp, int clientPort)
        {
            ServerThreadId = threadId;
            ClientIp = clientIp;
            ClientPort = clientPort;
        }

        public IPAddress ClientIp { get; }
        public int ClientPort { get; }
        public int ServerThreadId { get; }
    }
}