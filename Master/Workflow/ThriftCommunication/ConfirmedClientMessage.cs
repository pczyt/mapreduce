﻿namespace Master.Workflow.ThriftCommunication
{
    public class ConfirmedClientMessage : IClientThreadMessage
    {
        public ConfirmedClientMessage(int serverThreadId)
        {
            ServerThreadId = serverThreadId;
        }

        public int ServerThreadId { get; }
    }
}