﻿namespace Master.Workflow.ThriftCommunication
{
    public class DisconnectedClientThreadMessage : IClientThreadMessage
    {
        public DisconnectedClientThreadMessage(int id)
        {
            ServerThreadId = id;
        }

        public int ServerThreadId { get; }
    }
}