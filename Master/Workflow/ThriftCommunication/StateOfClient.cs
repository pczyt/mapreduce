﻿using System.Net;

namespace Master.Workflow.ThriftCommunication
{
    public class StateOfClient
    {
        public StateOfClient(IPAddress ip, int port, int queueNumber)
        {
            Ip = ip;
            ListeningPort = port;
            QueueNumber = queueNumber;
            CurrentClientState = CurrentClientState.Connected;
            Status = Status.Unconfirmed;
        }

        public IPAddress Ip { get; }

        public int ListeningPort { get; }

        //queuenumber jest jednoczesnie numerem watku odpowiedzialnego za wysylanie danych do workera
        public int QueueNumber { get; }
        public CurrentClientState CurrentClientState { get; set; }
        public Status Status { get; set; }
    }

    public enum Status
    {
        Unconfirmed,
        Confirmed
    }

    public enum CurrentClientState
    {
        Connected,
        Assigned,
        DuringMap,
        FinishedMap,
        DuringReduce,
        Finished
    }
}