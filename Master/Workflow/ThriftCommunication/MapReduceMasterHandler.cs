﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;
using MapReduce.Thrift;
using Master.Common;
using Master.Workflow.States;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.ThriftCommunication
{
    public class MapReduceMasterHandler : MapReduceMaster.Iface
    {
        private readonly ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> _dict;

        private readonly ILogger<MapReduceMasterHandler> _logger;

        //i
        private readonly BlockingCollection<IClientThreadMessage> _queue;
        private readonly ApplicationState _state;

        public MapReduceMasterHandler(ILogger<MapReduceMasterHandler> logger, ApplicationState state,
            BlockingCollection<IClientThreadMessage> queue,
            ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> dict)
        {
            _queue = queue;
            _logger = logger;
            _dict = dict;
            _state = state;
        }


        public bool RegisterWorker(int ip, int listeningPort)
        {
            if (!(_state.CurrentState is WorkersCollectingState)) return false;
            var currentThreadId = Thread.CurrentThread.ManagedThreadId;
            var ipAddress = new IPAddress(BitConverter.GetBytes(ip));
            _logger.LogDebug(
                $"Zarejestrowal sie worker. ThreadID={currentThreadId}\nZwrotne polaczenie: {ipAddress}:{listeningPort}");
            //utworzenie kolejki dla polaczenia zwrotnego
            _dict.AddOrUpdate(Thread.CurrentThread.ManagedThreadId,
                new BlockingCollection<SystemTask<MapReduceWorker.Client>>(), (x, y) => throw new InvalidState());
            //polecenie stworzenia polaczenia zwrotnego do klienta
            _queue.Add(new NewClientThreadMessage(currentThreadId, ipAddress, listeningPort));

            _state.ConnectedClients.AddOrUpdate(currentThreadId,
                new StateOfClient(ipAddress, listeningPort, currentThreadId), (x, z) => throw new InvalidState());
            return true;
        }


        public void Reconnect()
        {
            _logger.LogTrace($"Żądanie reconnect");
        }

        public void FinishedMap()
        {
            if (!(_state.CurrentState is MapState mapState)) return;

            _logger.LogDebug(
                $"Worker {_state.ConnectedClients.GetClientIp(Thread.CurrentThread.ManagedThreadId)} skonczyl map");
            mapState.FinishedMap(Thread.CurrentThread.ManagedThreadId);
        }

        public void RegisterResult(string key, string value)
        {
            if (!(_state.CurrentState is ReduceState reduceState)) return;
            _logger.LogDebug(
                $"Zarejestrowano wynik: {key} => {value} dla workera {_state.ConnectedClients.GetClientIp(Thread.CurrentThread.ManagedThreadId)}");
            _state.Results.Add(new Result((key, value)));
        }

        public void FinishedReduce()
        {
            if (!(_state.CurrentState is ReduceState reduceState)) return;
            _logger.LogDebug(
                $"Worker {_state.ConnectedClients.GetClientIp(Thread.CurrentThread.ManagedThreadId)} skonczyl reduce");
            reduceState.FinishedReduce(Thread.CurrentThread.ManagedThreadId);
        }
    }
}