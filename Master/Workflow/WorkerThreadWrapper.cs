﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Master.Workflow.Messages;
using Microsoft.Extensions.Logging;

namespace Master.Workflow
{
    //wrapper na potrzeby DI, aby logger i kolejki byly wstrzykiwane
//natomiast metoda Configure aby uniknac wstrzykiwania tokenu, nie chce zeby byl globalnie dostepny przez DI
    public class WorkerThreadWrapper
    {
        private readonly ILogger<WorkerThread> _logger;
        private readonly BlockingCollection<MasterWorkflowMessages> _masterQueue;
        private readonly BlockingCollection<Action<CancellationToken>> _workerQueue;

        public WorkerThreadWrapper(ILogger<WorkerThread> logger, BlockingCollection<MasterWorkflowMessages> masterQueue,
            BlockingCollection<Action<CancellationToken>> workerQueue)
        {
            _logger = logger;
            _masterQueue = masterQueue;
            _workerQueue = workerQueue;
        }

        public Thread Configure(CancellationToken token)
        {
            return new Thread(() => new WorkerThread(_logger, _workerQueue, _masterQueue).Start(token));
        }
    }
}