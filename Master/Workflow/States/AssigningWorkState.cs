﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using MapReduce.Thrift;
using Master.Common;
using Master.Utils;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class AssigningWorkState : StateBase
    {
        private readonly ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>>
            _clientsQueues;

        private readonly ILogger<AssigningWorkState> _logger;
     


        public AssigningWorkState(ILogger<AssigningWorkState> logger, ApplicationState state,
            ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> clientsQueues)
        {
            _logger = logger;
            _state = state;
            _clientsQueues = clientsQueues;
            
        }

        protected override ApplicationState _state { get; set; }

        public override void Do(CancellationToken token)
        {
            var clientsList = _state.ConnectedClients.GetClientListeningInfos();
            var i = 0;
            var dataPath = _state.Configuration.GeneralConfiguration.DataStoragePath;
            if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                var tmp = dataPath.Split(":/");
                dataPath = "/mnt/" + tmp[0].ToLower() + "/" + tmp[1];
            }
            
            var result = ShellRunner.Run($"wc -l < {dataPath}/{_state.MapReduceTask.DataFileName}", _state.Configuration.GeneralConfiguration.ShellPath);
            var linesPerWorker = Math.Ceiling((float.Parse(result[0])) / _state.ConnectedClients.Count);
           var t = ShellRunner.Run($"split -d -l {linesPerWorker} {dataPath}/{_state.MapReduceTask.DataFileName} {dataPath}/{_state.MapReduceTask.DataFileName}", _state.Configuration.GeneralConfiguration.ShellPath);
            foreach (var element in t)
            {
                Console.WriteLine(element);
            }          
            foreach (var element in _state.ConnectedClients)
            {
                _logger.LogTrace($"Dodaje do kolejki przydzial roboty dla ip {element.Value.Ip}");
                _clientsQueues[element.Value.QueueNumber].Add(new SystemTask<MapReduceWorker.Client>("AssignWork",
                    _state.MapReduceTask.DataFileName + $"{(i++).ToString().PadLeft(2,'0')}", _state.MapReduceTask.MapFileName,
                    _state.MapReduceTask.ReduceFileName, clientsList));
            }
        }
    }
}