﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using MapReduce.Thrift;
using Master.Common;
using Master.Configuration;
using Master.Utils;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;
using Shared.Network;

namespace Master.Workflow.States
{
    public class WorkersCollectingState : StateBase
    {
        private readonly ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>>
            _clientsQueues;

        private readonly object _lock = new object();
        private readonly ILogger<WorkersCollectingState> _logger;
        private readonly ILoggerFactory _loggerFactory;
        private readonly BlockingCollection<IClientThreadMessage> _newClientsThreadsQueue;
        private int _connectedClients;
        private bool _stop;

        public WorkersCollectingState(ILogger<WorkersCollectingState> logger, ApplicationState state,
            ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> clientsQueues,
            ILoggerFactory loggerFactory, BlockingCollection<IClientThreadMessage> newClientsThreadsQueue)
        {
            _logger = logger;
            _loggerFactory = loggerFactory;
            _state = state;
            ConnectedClients = 0;
            _clientsQueues = clientsQueues;
            _newClientsThreadsQueue = newClientsThreadsQueue;
        }

        protected override ApplicationState _state { get; set; }


        public int ConnectedClients
        {
            get => _connectedClients;
            set
            {
                _connectedClients = value;
                if (_state.Configuration != null) CheckCondition();
            }
        }

        private void CheckCondition()
        {
            if (_state.Configuration.WorkersCooperationConfiguration.StartCondition != StartCondition.WorkersNumber)
                throw new NotImplementedException();
            if (_state.Configuration.WorkersCooperationConfiguration.MinWorkers.Value > ConnectedClients) return;
            lock (_lock)
            {
                _stop = true;
            }
        }

        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;
            _logger.LogTrace($"{Name} running...");
            if (token.IsCancellationRequested)
            {
                _logger.LogTrace($"zatrzymywanie przez CancellationToken...");
                return;
            }

            var workersMulticastIp = _state.Configuration.WorkersCooperationConfiguration.MulticastGroupAddress;
            _logger.LogTrace($"Attempt to join {workersMulticastIp} group");
            UdpClient udpClient = null;
            try
            {
                udpClient = new UdpClient(AddressFamily.InterNetwork);
                var endPoint = new IPEndPoint(workersMulticastIp,
                    _state.Configuration.WorkersCooperationConfiguration.MulticastGroupPort);
                udpClient.JoinMulticastGroup(workersMulticastIp);
                while (!token.IsCancellationRequested && !_stop)
                {
                    _logger.LogTrace($"Wysylam {MulticastPackets.WorkersCollecting} na {endPoint}");
                    var sentBytes = udpClient.Send(MulticastPackets.WorkersCollecting.ToByteArray(),
                        MulticastPackets.WorkersCollecting.Length, endPoint);
                    _logger.LogTrace($"Wyslano {sentBytes} bajtow");
                    Thread.Sleep(5000);
                }

                _logger.LogTrace($"Koncze rozsylanie multicast");
            }
            catch (SocketException ex)
            {
                _logger.LogDebug($"Socket exception {ex.Message}\n {ex.StackTrace}");
            }
            finally
            {
                udpClient?.Dispose();
            }
        }

        public override void HandleDisconnectedClient(DisconnectedClientThreadMessage disconnectedMessage)
        {
            //w trakcie workers collecting state pozwalam na odlaczanie sie klientow
            _logger.LogTrace($"Rozlaczony worker{disconnectedMessage.ServerThreadId}");
            if (_state.ConnectedClients[disconnectedMessage.ServerThreadId].Status == Status.Confirmed)
                ConnectedClients--;
            _state.ConnectedClients.TryRemove(disconnectedMessage.ServerThreadId, out _);
        }

        public override void HandleNewClient(CancellationToken token, NewClientThreadMessage newClientThreadMessage,
            IClientThreadMessage element)
        {
            _logger.LogTrace(
                $"Uruchamiam watek {newClientThreadMessage.ClientIp}:{newClientThreadMessage.ClientPort} [{element.ServerThreadId}]");
            var threadClient = new Thread(() =>
                new ClientThread(_clientsQueues, newClientThreadMessage, _newClientsThreadsQueue,
                    _loggerFactory.CreateLogger<ClientThread>(), _state).Start(token));
            threadClient.Start();

            Task.Run(async () =>
            {
                try
                {
                    while (!token.IsCancellationRequested &&
                           _state.ConnectedClients.ContainsKey(element.ServerThreadId))
                    {
                        _clientsQueues[element.ServerThreadId].Add(new SystemTask<MapReduceWorker.Client>("Ping"));
                        await Task.Delay(5000, token);
                    }

                    _logger.LogTrace($"Koncze wysylac pingi dla id {element.ServerThreadId}");
                }
                catch (OperationCanceledException)
                {
                    _logger.LogTrace($"Pingowanie dla klienta {element.ServerThreadId} zakonczone");
                }
            }, token);
        }

        public override void HandleConfirmedMessage(ConfirmedClientMessage confirmedMessage)
        {
            _state.ConnectedClients[confirmedMessage.ServerThreadId].Status = Status.Confirmed;
            ConnectedClients++;
        }
    }
}