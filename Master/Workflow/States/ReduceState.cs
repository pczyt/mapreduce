﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using MapReduce.Thrift;
using Master.Common;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class ReduceState : StateBase
    {
        private readonly AutoResetEvent _allClientsFinished;

        private readonly ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>>
            _clientsQueues;

        private readonly ILogger<ReduceState> _logger;

        private int _finishedReduceCounter;

        public ReduceState(ILogger<ReduceState> logger, ApplicationState state,
            ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> clientsQueues)
        {
            _logger = logger;
            _state = state;
            _clientsQueues = clientsQueues;
            _allClientsFinished = new AutoResetEvent(false);
        }

        protected override ApplicationState _state { get; set; }

        public override void HandleDisconnectedClient(DisconnectedClientThreadMessage disconnectedMessage)
        {
            if (_state.ConnectedClients[disconnectedMessage.ServerThreadId].CurrentClientState !=
                CurrentClientState.Finished)
                throw new Exception($"Odlaczony worker {disconnectedMessage.ServerThreadId} w trakcie reduce!");
            _logger.LogDebug($"Odlaczono workera {disconnectedMessage.ServerThreadId} po skonczeniu reduce");
            _state.ConnectedClients.TryRemove(disconnectedMessage.ServerThreadId, out _);
        }

        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;

            _logger.LogTrace($"Jestem w {Name}");
            foreach (var client in _state.ConnectedClients)
            {
                client.Value.CurrentClientState = CurrentClientState.DuringReduce;
                _clientsQueues[client.Value.QueueNumber].Add(new SystemTask<MapReduceWorker.Client>("StartReduce"));
            }

            _allClientsFinished.WaitOne();
        }

        public void FinishedReduce(int currentThreadId)
        {
            _state.ConnectedClients[currentThreadId].CurrentClientState = CurrentClientState.Finished;
            Interlocked.Increment(ref _finishedReduceCounter);
            if (_finishedReduceCounter != _state.ConnectedClients.Count) return;
            _logger.LogTrace($"Wszyscy skonczyli reduce");
            //wszyscy skonczyli Reduce, czas na kolejny etap
            _allClientsFinished.Set();
        }
    }
}