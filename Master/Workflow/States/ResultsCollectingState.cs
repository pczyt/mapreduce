﻿using System;
using System.Threading;
using Master.Common;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class ResultsCollectingState : StateBase
    {
        private readonly ILogger<ResultsCollectingState> _logger;

        public ResultsCollectingState(ILogger<ResultsCollectingState> logger, ApplicationState state)
        {
            _logger = logger;
            _state = state;
        }

        protected override ApplicationState _state { get; set; }

        public override void HandleDisconnectedClient(DisconnectedClientThreadMessage disconnectedMessage)
        {
            _logger.LogInformation($"Odlaczono workera {disconnectedMessage.ServerThreadId}");
        }


        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;
          //  Console.WriteLine($"Robie {Name}");
            _logger.LogTrace($"Jestem w {Name}");
        }
    }
}