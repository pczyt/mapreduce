﻿using System.Collections.Concurrent;
using System.Threading;
using MapReduce.Thrift;
using Master.Common;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class MapState : StateBase
    {
        private readonly AutoResetEvent _allClientsFinished;

        private readonly ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>>
            _clientsQueues;

        private readonly ILogger<MapState> _logger;

        private int _finishedMapCounter;

        public MapState(ILogger<MapState> logger, ApplicationState state,
            ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>> clientsQueues)
        {
            _logger = logger;
            _state = state;
            _clientsQueues = clientsQueues;
            _allClientsFinished = new AutoResetEvent(false);
        }

        protected override ApplicationState _state { get; set; }


        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;

            foreach (var element in _state.ConnectedClients)
            {
                _logger.LogTrace($"Dodaje do kolejki StartMap dla ip {element.Value.Ip}");
                element.Value.CurrentClientState = CurrentClientState.DuringMap;
                _clientsQueues[element.Value.QueueNumber].Add(new SystemTask<MapReduceWorker.Client>("StartMap"));
            }

            _allClientsFinished.WaitOne();
        }

        public void FinishedMap(int currentThreadId)
        {
            _state.ConnectedClients[currentThreadId].CurrentClientState = CurrentClientState.FinishedMap;
            Interlocked.Increment(ref _finishedMapCounter);
            if (_finishedMapCounter == _state.ConnectedClients.Count) _allClientsFinished.Set();
        }
    }
}