﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Master.API.Controllers;
using Master.Common;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class OnlyApiState : StateBase
    {
        private readonly ILogger<OnlyApiState> _logger;
        private readonly BlockingCollection<MapReduceTask> _queue;

        public OnlyApiState(ILogger<OnlyApiState> logger, ApplicationState state,
            BlockingCollection<MapReduceTask> queue)
        {
            _logger = logger;
            _state = state;
            _queue = queue;
        }

        protected override ApplicationState _state { get; set; }


        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;
            _logger.LogTrace($"Czekam na zadanie z API");
            try
            {
                foreach (var element in _queue.GetConsumingEnumerable(token))
                {
                    _state.MapReduceTask = element;
                    break;
                }
            }
            catch (OperationCanceledException)
            {
                _logger.LogTrace($"Zatrzymuje OnlyApiState");
            }
        }
    }
}