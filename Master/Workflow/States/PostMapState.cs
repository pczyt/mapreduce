﻿using System;
using System.Threading;
using Master.Common;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class PostMapState : StateBase
    {
        private readonly ILogger<PostMapState> _logger;

        public PostMapState(ILogger<PostMapState> logger, ApplicationState state)
        {
            _logger = logger;
            _state = state;
        }

        protected override ApplicationState _state { get; set; }


        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;
           // Console.WriteLine($"Robie {Name}");
            _logger.LogTrace($"Jestem w {Name}");
        }
    }
}