﻿using System;
using System.Threading;
using Master.Common;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public abstract class StateBase : IState
    {
        private ILogger _logger;
        protected abstract ApplicationState _state { get; set; }

        public string Name => GetType().Name;
        public abstract void Do(CancellationToken token);

        public virtual void HandleDisconnectedClient(DisconnectedClientThreadMessage disconnectedMessage)
        {
            _state.RunningState = RunningState.Aborted;
            throw new Exception(
                $"Utracone polaczenie z workerem w stanie {_state.CurrentState.Name}{disconnectedMessage.ServerThreadId}!");
        }

        public virtual void HandleNewClient(CancellationToken token, NewClientThreadMessage newClientThreadMessage,
            IClientThreadMessage element)
        {
            _logger.LogTrace("Proba podlaczenia sie workera poza stanem zbierania workerow");
        }

        public virtual void HandleConfirmedMessage(ConfirmedClientMessage confirmedMessage)
        {
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}