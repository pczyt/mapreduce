﻿using System;
using System.IO;
using System.Threading;
using Master.Common;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow.States
{
    public class FinishedState : StateBase
    {
        private readonly ILogger<FinishedState> _logger;

        public FinishedState(ILogger<FinishedState> logger, ApplicationState state)
        {
            _logger = logger;
            _state = state;
        }

        protected override ApplicationState _state { get; set; }


        public override void Do(CancellationToken token)
        {
            _state.CurrentState = this;
            var resultsFileName = DateTime.Now.ToString("d_M_yyyy_HH_mm_ss");
            var resultFile = File.Create($"Results-{resultsFileName}");
            var writer = new StreamWriter(resultFile);
            foreach (var pair in _state.Results)
            {
                _logger.LogTrace($"{pair.Key} => {pair.Value}");
                writer.WriteLine($"{pair.Key},{pair.Value}");
            }

            _logger.LogDebug($"Wyniki zapisane w pliku {resultsFileName}");
            writer.Close();
            resultFile.Close();
            WaitHandle.WaitAny(new[] {token.WaitHandle});
        }


        public override void HandleDisconnectedClient(DisconnectedClientThreadMessage disconnectedMessage)
        {
            _logger.LogTrace($"Worker {disconnectedMessage.ServerThreadId} odlaczony");
            _state.ConnectedClients.TryRemove(disconnectedMessage.ServerThreadId, out _);
        }
    }
}