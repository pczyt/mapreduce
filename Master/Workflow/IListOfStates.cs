﻿using System.Collections.Generic;

namespace Master.Workflow
{
    public interface IListOfStates
    {
        List<IState> List { get; }
    }
}