﻿using System.Threading;
using Master.Workflow.ThriftCommunication;

namespace Master.Workflow
{
    public interface IState
    {
        string Name { get; }
        void Do(CancellationToken token);
        void HandleDisconnectedClient(DisconnectedClientThreadMessage disconnectedMessage);

        void HandleNewClient(CancellationToken token, NewClientThreadMessage newClientThreadMessage,
            IClientThreadMessage element);

        void HandleConfirmedMessage(ConfirmedClientMessage confirmedMessage);
    }
}