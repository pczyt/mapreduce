﻿using System.Collections.Generic;
using Master.Workflow.States;

namespace Master.Workflow
{
    public class MasterListOfStates : IListOfStates
    {
        //brzydki konstruktor na potrzeby DI, nie zmieniac typow argumentow na IState
        public MasterListOfStates(OnlyApiState s1, WorkersCollectingState s2, AssigningWorkState s3, MapState s4,
            PostMapState s5,
            ReduceState s6, ResultsCollectingState s7, FinishedState s8)
        {
            List = new List<IState>(7) {s1, s2, s3, s4, s5, s6, s7, s8};
        }

        public List<IState> List { get; }
    }
}