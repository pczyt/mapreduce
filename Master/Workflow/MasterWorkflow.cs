﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Master.Common;
using Master.Workflow.Messages;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;

namespace Master.Workflow
{
    public class MasterWorkflow
    {
        private readonly ApplicationState _applicationState;
        private readonly ILogger<MasterWorkflow> _logger;
        private readonly BlockingCollection<MasterWorkflowMessages> _masterQueue;
        private readonly ServerThread _serverThread;
        private readonly List<IState> _states;
        private readonly BlockingCollection<Action<CancellationToken>> _workerQueue;
        private readonly WorkerThreadWrapper _workerThreadWrapper;

        public MasterWorkflow(ILogger<MasterWorkflow> logger, IListOfStates wrapper,
            BlockingCollection<MasterWorkflowMessages> masterQueue,
            BlockingCollection<Action<CancellationToken>> workerQueue, WorkerThreadWrapper threadWrapper,
            ServerThread serverThread, ApplicationState appState)
        {
            if (wrapper.List.Count == 0) throw new ArgumentException("List of states cannot be empty!");
            _states = wrapper.List;
            _logger = logger;
            _masterQueue = masterQueue;
            _workerQueue = workerQueue;
            _workerThreadWrapper = threadWrapper;
            _serverThread = serverThread;
            _applicationState = appState;
        }

        public void Start(CancellationTokenSource tokenSource)
        {
            _logger.LogTrace($"Running MasterWorkFlow");

            var token = tokenSource.Token;

            var workerThread = _workerThreadWrapper.Configure(token);
            workerThread.Start();
            var serverThread = new Thread(() => _serverThread.Start(token));
            serverThread.Start();
            foreach (var state in _states)
            {
                _logger.LogDebug($"Attempt to run state {state.Name}");
                _workerQueue.Add(x => state.Do(x));
                _logger.LogDebug($"Waiting for end state {state.Name}");
                var result = _masterQueue.Take();
                if (result == MasterWorkflowMessages.Abort || _masterQueue.Contains(MasterWorkflowMessages.Abort) ||
                    token.IsCancellationRequested)
                {
                    _logger.LogDebug($"Abortowano z zewnatrz. Koncze watek");
                    _applicationState.RunningState = RunningState.Aborted;
                    tokenSource.Cancel();
                    _serverThread.CleanUp();
                    //czekam aby watek zdazyl sie pozamykac i zalogowac wszystko
                    workerThread.Join(5000);
                    return;
                }

                _logger.LogDebug($"Finished state {state.Name}");
            }

            //zwijam wszystko
            tokenSource.Cancel();
            _serverThread.CleanUp();
            Thread.Sleep(500);
        }
    }
}