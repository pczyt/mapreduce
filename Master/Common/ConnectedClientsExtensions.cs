﻿using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using Master.Workflow.ThriftCommunication;

namespace Master.Common
{
    public static class ConnectedClientsExtensions
    {
        public static IPAddress GetClientIp(this ConcurrentDictionary<int, StateOfClient> dic, int threadId)
        {
            return dic.Where(x => x.Value.QueueNumber == threadId).Select(x => x.Value.Ip).First();
        }
    }
}