﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using Master.Configuration;
using Master.Workflow;
using Master.Workflow.ThriftCommunication;

namespace Master.Common
{
    public class ApplicationStateReadOnly : IApplicationState
    {
        private readonly IApplicationState _applicationStateImplementation;

        public ApplicationStateReadOnly(IApplicationState state)
        {
            _applicationStateImplementation = state;
        }

        public IConfiguration Configuration
        {
            get => _applicationStateImplementation.Configuration;
            set => throw new ReadOnlyException("You can't modify ReadOnly Application State");
        }

        public IState CurrentState
        {
            get => _applicationStateImplementation.CurrentState;
            set => throw new ReadOnlyException("You can't modify ReadOnly ApplicatonState");
        }

        public ConcurrentDictionary<int, StateOfClient> ConnectedClients =>
            _applicationStateImplementation.ConnectedClients;

        public RunningState RunningState
        {
            get => _applicationStateImplementation.RunningState;
            set => throw new ReadOnlyException("You can't modify ReadOnly ApplicatonState");
        }

        public ConcurrentBag<Result> Results
        {
            get => _applicationStateImplementation.Results;
            set => throw new ReadOnlyException("You can't modify ReadOnly ApplicatonState");
        }
    }
}