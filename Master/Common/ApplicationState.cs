﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using Master.API.Controllers;
using Master.Configuration;
using Master.Workflow;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.Logging;
using SQLitePCL;

namespace Master.Common
{
    public class ApplicationState : IApplicationState
    {
        private readonly ILogger<ApplicationState> _logger;
        private IConfiguration _configuration;
        private IState _currentState;

        public ApplicationState(ILogger<ApplicationState> logger)
        {
            _logger = logger;
            ConnectedClients = new ConcurrentDictionary<int, StateOfClient>();
            RunningState = RunningState.Running;
            Results = new ConcurrentBag<Result>();
        }

        public MapReduceTask MapReduceTask { get; set; }

        public IConfiguration Configuration
        {
            get => _configuration;
            set
            {
                if (_configuration == null) _configuration = value;
                else
                    throw new ReadOnlyException("Configuration is already set. Configuration is read only now.");
            }
        }

        public IState CurrentState
        {
            get => _currentState;
            set
            {
                _logger.LogTrace($"Changing state from {_currentState} to {value}");
                _currentState = value;
            }
        }

        public ConcurrentDictionary<int, StateOfClient> ConnectedClients { get; }
        public RunningState RunningState { get; set; }
        public ConcurrentBag<Result> Results { get; set; }
    }

    public struct Result
    {
        public string Key { get; }
        public string Value { get; }

        public Result(ValueTuple<string, string> tuple)
        {
            Key = tuple.Item1;
            Value = tuple.Item2;
        }
    }
    public enum RunningState
    {
        Running,
        Aborted
    }
}