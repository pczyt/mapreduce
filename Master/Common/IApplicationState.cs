﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Master.Configuration;
using Master.Workflow;
using Master.Workflow.ThriftCommunication;

namespace Master.Common
{
    public interface IApplicationState
    {
        IConfiguration Configuration { get; set; }
        IState CurrentState { get; set; }
        ConcurrentDictionary<int, StateOfClient> ConnectedClients { get; }
        RunningState RunningState { get; set; }
        ConcurrentBag<Result> Results { get; set; }
    }
}