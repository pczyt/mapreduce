﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using MapReduce.Thrift;
using Master.API.Controllers;
using Master.Common;
using Master.Configuration;
using Master.Utils;
using Master.Workflow;
using Master.Workflow.Messages;
using Master.Workflow.States;
using Master.Workflow.ThriftCommunication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using LogLevel = NLog.LogLevel;

namespace Master
{
    internal class Program
    {
        private static string _configFile = "config.json";

        private static void Main(string[] args)
        {
            var tokenSource = new CancellationTokenSource();
            var logger = LogManager.LoadConfiguration("nlog.config").GetCurrentClassLogger();
            Console.CancelKeyPress += delegate { tokenSource.Cancel(); };
            try
            {
                _configFile = ReadConfigFilePath(args) ?? _configFile;
                var servicesProvider = BuildDi();
                var runner = servicesProvider.GetRequiredService<Core>();


                runner.Start(tokenSource);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                logger.Log(LogLevel.Debug, "Zamykam aplikacje.");
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                LogManager.Shutdown();
            }
        }

        private static string ReadConfigFilePath(string[] args)
        {
            if (args.Length == 1) return args[0];
            PrintHelp();
            return null;
        }

        private static void PrintHelp()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("first and only argument - path to json config file");
            Console.WriteLine("default: config.json");
        }

        private static IServiceProvider BuildDi()
        {
            var services = new ServiceCollection();

            //Runner is the custom class
            services.AddSingleton<Core>();
            services.AddSingleton<IConfigurationProvider>(new JsonConfigurationProvider(File.ReadAllText(_configFile)));
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddLogging(builder => builder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace));
            services.AddSingleton<ApplicationState>();

            #region tworzenie stanow aplikacji

            services.AddSingleton<OnlyApiState>();
            services.AddSingleton<WorkersCollectingState>();
            services.AddSingleton<AssigningWorkState>();
            services.AddSingleton<MapState>();
            services.AddSingleton<PostMapState>();
            services.AddSingleton<ReduceState>();
            services.AddSingleton<ResultsCollectingState>();
            services.AddSingleton<FinishedState>();

            #endregion

            services.AddSingleton<IListOfStates, MasterListOfStates>();
            services.AddSingleton<MasterWorkflow>();
            services.AddSingleton(
                new BlockingCollection<MasterWorkflowMessages>());
            services.AddSingleton(new BlockingCollection<Action<CancellationToken>>());
            services.AddSingleton(new BlockingCollection<IClientThreadMessage>());
            services.AddSingleton(
                new ConcurrentDictionary<int, BlockingCollection<SystemTask<MapReduceWorker.Client>>>());
            services.AddSingleton<ServerThread>();
            services.AddSingleton<MapReduceMasterHandler>();
            services.AddSingleton<WorkerThreadWrapper>();
            services.AddSingleton<BlockingCollection<MapReduceTask>>();
           

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions
            {
                CaptureMessageTemplates = true,
                CaptureMessageProperties = true
            });
            return serviceProvider;
        }
    }
}