﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Master.API;
using Master.API.Controllers;
using Master.Common;
using Master.Configuration;
using Master.Workflow;
using Master.Workflow.Messages;
using Microsoft.Extensions.Logging;

namespace Master
{
    public class Core
    {
        private readonly ApplicationState _appState;
        private readonly IConfiguration _configuration;
        private readonly ILogger<Core> _logger;
        private readonly BlockingCollection<MasterWorkflowMessages> _masterQueue;
        private readonly MasterWorkflow _workflow;
        private readonly BlockingCollection<MapReduceTask> _tasksQueue;


        public Core(ILogger<Core> logger, IConfigurationProvider configurationProvider, MasterWorkflow workflow,
            ApplicationState appState, BlockingCollection<MasterWorkflowMessages> queue,
            BlockingCollection<MapReduceTask> tasksQueue)
        {
            _logger = logger;
            _configuration = configurationProvider.GetConfiguration();
            _appState = appState;
            _appState.Configuration = _configuration;
            _workflow = workflow;
            _masterQueue = queue;
            _tasksQueue = tasksQueue;
        }

        public void Start(CancellationTokenSource tokenSource)
        {
            _logger.LogDebug("Uruchomiono program.");
            _logger.LogDebug(_configuration.ToString());
            _logger.LogTrace($"Uruchamiam API na oddzielnym watku");
            var apiThread =
                new Thread(() => ApiConfigurer.Start(_masterQueue, _tasksQueue, new ApplicationStateReadOnly(_appState),
                    tokenSource.Token));
            apiThread.Start();

            _logger.LogTrace("Uruchamiam modul workflow");
            try
            {
                _workflow.Start(tokenSource);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(
                    $"Wystapil wyjatek {ex.Message} \n {ex.StackTrace} \n Aplikacja zostanie zamknieta.");
            }

            _logger.LogTrace("Modul workflow zakonczony");
        }
    }
}