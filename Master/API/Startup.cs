﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;

namespace Master.API
{
    //modyfikowana klasa, nie nadpisywac oryginalem
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
                .AddBasicAuthentication(
                    options =>
                    {
                        options.Realm = "MapReduce WebApi";

                        options.Events = new BasicAuthenticationEvents
                        {
                            OnValidatePrincipal = context =>
                            {
                                if (context.UserName != ApiConfigurer.State.Configuration.WebApiConfiguration.Login ||
                                    context.Password != ApiConfigurer.State.Configuration.WebApiConfiguration.Password)
                                    return Task.FromResult(AuthenticateResult.Fail("Authentication failed."));
                                var claims = new List<Claim>
                                {
                                    new Claim(ClaimTypes.Name,
                                        context.UserName,
                                        context.Options.ClaimsIssuer)
                                };

                                var ticket = new AuthenticationTicket(
                                    new ClaimsPrincipal(new ClaimsIdentity(
                                        claims,
                                        BasicAuthenticationDefaults.AuthenticationScheme)),
                                    new AuthenticationProperties(),
                                    BasicAuthenticationDefaults.AuthenticationScheme);

                                return Task.FromResult(AuthenticateResult.Success(ticket));
                            }
                        };
                    });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new IPAddressConverter());
            });

            services.Configure<MvcOptions>(options => { options.Filters.Add(new RequireHttpsAttribute()); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            /* potrzebne, gdy sluchamy tez nieszyfrowanego ruchu http
             var options = new RewriteOptions()
                      .AddRedirectToHttps(StatusCodes.Status301MovedPermanently, ApiConfigurer.State.Configuration.WebApiConfiguration.Port);
                  app.UseRewriter(options);
              */

            app.UseMiddleware<BasicAuthMiddleware>("MapReduce WebApi");
            app.UseMvc();
        }
    }

//bez tego serializacja IPAddress ipv4 powoduje wyjatek
    public class IPAddressConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IPAddress);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            return IPAddress.Parse((string) reader.Value);
        }
    }
}