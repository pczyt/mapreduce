﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading;
using Master.API.Controllers;
using Master.Common;
using Master.Workflow.Messages;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Master.API
{
    public static class ApiConfigurer
    {
        public static BlockingCollection<MasterWorkflowMessages> WorkFlowQueue;
        public static IApplicationState State { get; private set; }

        public static BlockingCollection<MapReduceTask> TasksQueue { get; set; }

        public static void Start(BlockingCollection<MasterWorkflowMessages> queue,
            BlockingCollection<MapReduceTask> tasksQueue, IApplicationState state, CancellationToken token)
        {
            WorkFlowQueue = queue;
            State = state;
            TasksQueue = tasksQueue;
            var webHost = BuildWebHost();
            var t = webHost.RunAsync(token);
            t.Wait();
            Console.WriteLine("Zawijam API!");
        }


        private static IWebHost BuildWebHost()
        {
            return WebHost.CreateDefaultBuilder()
                .UseKestrel(options =>
                {
                    options.Listen(IPAddress.Parse(State.Configuration.WebApiConfiguration.Host), State.Configuration.WebApiConfiguration.Port,
                        listenOptions =>
                        {
                            listenOptions.UseHttps(State.Configuration.WebApiConfiguration.CertificatePath,
                                State.Configuration.WebApiConfiguration.CertificatePassword);
                        });
                })
                .UseStartup<Startup>()
                .Build();
        }
    }
}