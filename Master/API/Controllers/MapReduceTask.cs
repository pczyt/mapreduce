﻿namespace Master.API.Controllers
{
    public class MapReduceTask
    {
        public string DataFileName { get; set; }
        public string MapFileName { get; set; }
        public string ReduceFileName { get; set; }
    }
}