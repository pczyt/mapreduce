﻿using Microsoft.AspNetCore.Mvc;

namespace Master.API.Controllers
{
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        public static bool TaskAssigned;

        [HttpPost]
        public IActionResult Add([FromBody] MapReduceTask item)
        {
            if (item?.DataFileName == null || item.MapFileName == null || item.ReduceFileName == null || TaskAssigned)
                return BadRequest();
            if (!System.IO.File.Exists(ApiConfigurer.State.Configuration.GeneralConfiguration.DataStoragePath + "/" +
                                       item.MapFileName))
                return BadRequest("MapFile not found");
            if (!System.IO.File.Exists(ApiConfigurer.State.Configuration.GeneralConfiguration.DataStoragePath + "/" +
                                       item.ReduceFileName))
                return BadRequest("ReduceFile not found");
            if (!System.IO.File.Exists(ApiConfigurer.State.Configuration.GeneralConfiguration.DataStoragePath + "/" +
                                       item.DataFileName))
                return BadRequest("DataFile not found");
            ApiConfigurer.TasksQueue.Add(item);
            TaskAssigned = true;
            return Ok("Task was assigned");
        }
    }
}