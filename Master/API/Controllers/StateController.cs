﻿using Master.Workflow.Messages;
using Microsoft.AspNetCore.Mvc;

namespace Master.API.Controllers
{
    [Route("api/[controller]")]
    public class StateController : Controller
    {
        [HttpPost]
        public IActionResult Abort([FromBody] AbortRequest item)
        {
            if (item == null || !item.Abort)
                return BadRequest();
            ApiConfigurer.WorkFlowQueue.Add(MasterWorkflowMessages.Abort);
            return Ok("Abort message sent.");
        }


        [HttpGet]
        public IActionResult State()
        {
            return Ok(ApiConfigurer.State);
        }

        public class AbortRequest
        {
            public bool Abort { get; set; }
        }

        public class StartRequest
        {
            public bool Start { get; set; }
        }
    }
}